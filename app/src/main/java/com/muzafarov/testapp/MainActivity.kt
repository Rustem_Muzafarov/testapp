package com.muzafarov.testapp

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener

class MainActivity : AppCompatActivity() {

    private val request = "{\n" +
            "   \"event\":\"subscribe\",\n" +
            "   \"channel\":\"ticker\",\n" +
            "   \"pair\":\"BTCUSD\"\n" +
            "}"

    val list = mutableListOf<Entry>()
    val bidList = mutableListOf<Float>()

    private val listener = object: WebSocketListener() {
        override fun onOpen(webSocket: WebSocket?, response: Response?) {
            webSocket?.send(request)
            super.onOpen(webSocket, response)
        }

        override fun onFailure(webSocket: WebSocket?, t: Throwable?, response: Response?) {
            Toast
                    .makeText(this@MainActivity, t?.message,
                            Toast.LENGTH_SHORT)
                    .show()
            super.onFailure(webSocket, t, response)
        }

        override fun onMessage(webSocket: WebSocket?, text: String?) {
            Log.d("TAG", text)
            text?.let {
                val bid = getBid(it)
                if(bid != null) {
                    val entry = Entry(list.size.toFloat(), bid)
                    list.add(entry)
                    addPoint(entry)
                }
            }
            super.onMessage(webSocket, text)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        WebSocketCreator.createService(listener)
        lineChart.setBackgroundColor(Color.WHITE)
        lineChart.setScaleEnabled(true)
        lineChart.isDragEnabled = true
        lineChart.setPinchZoom(true)
    }

    private fun addPoint(entry: Entry) {
        if(lineChart.data != null &&
                lineChart.data.dataSetCount > 0) {
            val set = lineChart.data.getDataSetByIndex(0)
            set.addEntry(entry)
            lineChart.data.notifyDataChanged()
            lineChart.notifyDataSetChanged()
        } else {
            val set = LineDataSet(mutableListOf(entry), "DataSet")
            set.color = Color.BLACK
            set.lineWidth = 1F
            val dataSets = ArrayList<ILineDataSet>()
            dataSets.add(set)
            val data = LineData(dataSets)
            lineChart.data = data
        }
        lineChart.invalidate()
    }

    private fun getBid(text: String): Float? {
        val data = text.split(",")
        if(data.size <= 2)  return bidList[bidList.size - 1]

        val bid = data[1].toFloatOrNull()
        bid?.let {
            bidList.add(it)
        }
        return bid
    }
}
