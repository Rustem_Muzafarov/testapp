package com.muzafarov.testapp

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

// Created by Rustem Muzafarov.
// Onza.ME(c) All rights reserved.

object WebSocketCreator {

    fun createService(listener: WebSocketListener): WebSocket {
        val logInterceptor = HttpLoggingInterceptor()
                .apply { level = HttpLoggingInterceptor.Level.BODY }

        val client = OkHttpClient.Builder().apply {
            addInterceptor(logInterceptor)
        }
                .readTimeout(3, TimeUnit.SECONDS)
                .build()

        val request = Request.Builder()
                .url("wss://api.bitfinex.com/ws/pubticker/btcusd")
                .build()

        val socket = client.newWebSocket(request, listener)
        client.dispatcher().executorService().shutdown()

        return socket
    }
}