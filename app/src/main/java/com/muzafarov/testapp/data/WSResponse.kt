package com.muzafarov.testapp.data

// Created by Rustem Muzafarov.
// Onza.ME(c) All rights reserved.

data class WSResponse(
        val channelId: Int,
        val bid: Float,
        val bidSize: Float,
        val ask: Float,
        val askSize: Float,
        val dailyChange: Float,
        val dailyChangePerc: Float,
        val lastPrice: Float,
        val volume: Float,
        val high: Float,
        val low: Float)